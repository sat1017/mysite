# multi build stages
#https://docs.docker.com/develop/develop-images/multistage-build/

# ==================================== BASE ====================================
FROM python:3.8-slim-buster AS base

WORKDIR /mysite

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP mysite.app
ENV FLASK_ENV development
ENV FLASK_DEBUG 1
# https://stackoverflow.com/questions/64219058/docker-compose-ports-not-mapping-logs-shows-app-is-running/64219511#64219511
# https://stackoverflow.com/questions/30323224/deploying-a-minimal-flask-app-in-docker-server-connection-issues
ENV FLASK_RUN_HOST=0.0.0.0


RUN apt-get update
RUN apt-get install -y \
	gcc python3-dev

RUN pip install --upgrade pip
COPY . .
# if using user and path add --user flag to pip install lines
#RUN useradd -m sid
#RUN chown -R sid:sid /mysite
#USER sid
#ENV PATH="/home/sid/.local/bin:${PATH}"

# ================================= DEVELOPMENT ================================
# FROM base as development
# COPY ./requirements-dev.txt /personal_site/requirements-dev.txt
# RUN pip install -r requirements-dev.txt
# EXPOSE 5000
# CMD flask run

# =================================== MANAGE ===================================
# FROM base as manage
# COPY ./requirements-dev.txt /personal_site/requirements-dev.txt
# RUN pip install -r requirements-dev.txt

# ================================= PRODUCTION =================================
FROM base as production
COPY ./requirements-prod.txt /personal_site/requirements-prod.txt
RUN pip install -r requirements-prod.txt
# uncomment if using locally
EXPOSE 5000
ENV FLASK_ENV production
ENV FLASK_DEBUG 0
# uncomment if using heroku; heroku uses this cmd to restart services instead of procfile
# TODO: figure out why procfile isn't being used
CMD gunicorn -w 4 -b 0.0.0.0:$PORT --access-logfile - "mysite.app:create_app()"
# uncomment if using locally
# CMD gunicorn -w 4 -b 0.0.0.0:5000 --access-logfile - "mysite.app:create_app()"