"""Main application package."""
import datetime

from flask import Flask, g

from mysite import public


def create_app(config_object="mysite.settings"):
    """
    :param config_object: The configuration object to use.
    """

    app = Flask(__name__)
    app.config.from_object(config_object)
    register_blueprints(app)
    return app


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(public.views.blueprint)
    return None
