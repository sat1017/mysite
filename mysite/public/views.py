import datetime

from flask import Blueprint, current_app, g, render_template, url_for

blueprint = Blueprint('public', __name__)

cur_year = datetime.date.today().year


@blueprint.route('/')
def home():
    return render_template('index.html')


@blueprint.route('/projects')
def projects_home():
    return render_template('projects.html')


@blueprint.route('/resume')
def resume_home():
    return render_template('resume.html')
