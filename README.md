# ABOUT:
This is the source code for my personal website.

# DEVOPS COMMANDS:
```
sudo sudo heroku login
sudo heroku container:login
sudo heroku config:set SECRET_KEY=SOME_SECRET_KEY --app <heroku_app_name>
sudo docker build -f Dockerfile -t registry.heroku.com/<heroku_app_name>/web .
sudo docker push registry.heroku.com/<heroku_app_name>/web:latest
heroku container:release web --app <heroku_app_name>
```
```
source venv/bin/activate
sudo docker-compose up
sudo sudo heroku login
sudo heroku container:login
heroku container:push web
heroku container:release web
```
